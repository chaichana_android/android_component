package buu.chaichana.kotlinandroidtutorial

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class ListViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)

        val arrayAdapter : ArrayAdapter<*>
        val users = arrayOf(
            "Virat Kohil", "Rohit Sharma", "Steve Smith", "Kane Williamson", "Ross Taylor",
            "Virat Kohil", "Rohit Sharma", "Steve Smith", "Kane Williamson", "Ross Taylor",
            "Virat Kohil", "Rohit Sharma", "Steve Smith", "Kane Williamson", "Ross Taylor",
            "Virat Kohil", "Rohit Sharma", "Steve Smith", "Kane Williamson", "Ross Taylor"
        )
        val userList = findViewById<ListView>(R.id.userList)
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, users)
        userList.adapter = arrayAdapter
        userList.setOnItemClickListener { parent, view, position, id ->
            Toast.makeText(this@ListViewActivity, "" + users[position], Toast.LENGTH_LONG).show()
            val intent = Intent(this@ListViewActivity, ShowNameActivity::class.java)
            intent.putExtra("name",users[position])
            startActivity(intent)

        }

    }
}
